// Package sys -
package sys

import "runtime"

// ===== [ Constants and Variables ] =====

// ===== [ Types ] =====

// ===== [ Implementations ] =====

// ===== [ Private Functions ] =====

// ===== [ Public Functions ] =====

// GetCPUCount - 현재 실행중인 머신의 CPU 갯수 반환
func GetCPUCount() int {
	return runtime.NumCPU()
}
