module gitlab.com/ccambo/msfl-go-core

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	go.uber.org/zap v1.14.0
	golang.org/x/tools v0.0.0-20200212213342-7a21e308cf6c // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
