// Package util -
package util

import "math/rand"

// ===== [ Constants and Variables ] =====

// ===== [ Types ] =====

// ===== [ Implementations ] =====

// ===== [ Private Functions ] =====

// ===== [ Public Functions ] =====

// RandomInt - 지정한 Range 구간의 정수 랜덤값 반환
func RandomInt(start int, end int) int {
	return rand.Intn(end-start) + start
}
