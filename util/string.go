// Package util -
package util

import "strings"

// ===== [ Constants and Variables ] =====

// ===== [ Types ] =====

// ===== [ Implementations ] =====

// ===== [ Private Functions ] =====

// ===== [ Public Functions ] =====

// StartsWith - 지정된 val 값이 지정된 prefix 값으로 시작되는지를 검증한다.
func StartsWith(val string, prefix string) bool {
	return strings.HasPrefix(val, prefix)
}
