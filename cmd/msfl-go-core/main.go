// Package main -
package main

import (
	"log"

	"gitlab.com/ccambo/msfl-go-core/logger"
	"gitlab.com/ccambo/msfl-go-core/util"
)

// ===== [ Constants and Variables ] =====

// ===== [ Types ] =====

// ===== [ Implementations ] =====

// ===== [ Private Functions ] =====

func main() {
	// Sets the Common logging
	logConf := logger.Config{
		EnableConsole:     true,
		ConsoleLevel:      logger.LevelDebug,
		ConsoleJSONFormat: true,
		EnableFile:        true,
		FileLevel:         logger.LevelInfo,
		FileJSONFormat:    true,
		FileLocation:      "log.log",
	}

	err := logger.NewLogger(logConf, logger.InstanceLogrusLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %ss", err.Error())
	}

	logger.Info("MSFL Core Library for GO!!")
	logger.Infof("RadomInt %d", util.RandomInt(0, 100))
}

// ===== [ Public Functions ] =====
